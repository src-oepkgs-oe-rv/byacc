Name:           byacc
Version:        2.0.20210109
Release:        1
Summary:        A parser generator
License:        public domain
URL:            https://invisible-island.net/byacc/byacc.html
Source0:        https://invisible-island.net/datafiles/release/byacc.tar.gz

BuildRequires:  gcc

%description
Berkeley Yacc is an LALR(1) parser generator.  Berkeley Yacc has been made
as compatible as possible with AT&T Yacc.  Berkeley Yacc can accept any input
specification that conforms to the AT&T Yacc documentation.  Specifications
that take advantage of undocumented features of AT&T Yacc will probably be
rejected.

%package_help

%prep
%autosetup -n byacc-20210109 -p1
find . -type f -name \*.c -print0 | xargs -0 sed -i 's/YYSTACKSIZE 500/YYSTACKSIZE 10000/g'

%build
%configure --disable-dependency-tracking
%make_build

%install
%make_install
ln -s yacc %{buildroot}%{_bindir}/byacc
ln -s yacc.1 %{buildroot}%{_mandir}/man1/byacc.1

%check
#make check

%files
%doc ACKNOWLEDGEMENTS README* NO_WARRANTY
%license AUTHORS
%{_bindir}/*

%files help
%doc CHANGES NOTES
%{_mandir}/man1/*

%changelog
* Mon Feb 1 2021 wangjie<wangjie294@huawei.com> - 2.0.20210109
- DESC:upgrade 2.0.20210109

* Sta Jul 25 2020 xinghe <xinghe1@huawei.com> - 1.9.20200330-1
- update version to 1.9.20200330

* Wed Jan 22 2020 gulining<gulining1@huawei.com> - 1.9.20170709-9
- Disable test

* Fri Dec 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.20170709-8
- Add help package

* Wed Dec 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.20170709-7
- Package init
